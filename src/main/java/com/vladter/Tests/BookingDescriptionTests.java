package com.vladter.Tests;

import com.vladter.Model.BookingClasses.BookingDescription;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.RoomClasses.RoomType;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class BookingDescriptionTests {

    @Test
    public void testConstructorCorrect() {
        RoomType roomType = makeBookingRoom();
        BookingDescription bookingDescription = new BookingDescription(roomType, RoomKind.SUITE, new BigDecimal(3.0),1);

        assertSame(bookingDescription.getRoomType(), roomType);
        assertEquals(bookingDescription.getRoomKind(), RoomKind.SUITE);
        assertEquals(bookingDescription.getFixedPrice(), new BigDecimal(3.0));
        assertEquals(bookingDescription.getAmount(), 1);

    }

    @Test(expected = Exception.class)
    public void testNonPositivePriceFails() {
        new BookingDescription(makeBookingRoom(), RoomKind.SUITE, new BigDecimal(-0.01), 1);
        new BookingDescription(makeBookingRoom(), RoomKind.SUITE, new BigDecimal(0.00), 1);
    }

    @Test
    public void testMultiplyPrice() {
        BookingDescription bookingDescription = new BookingDescription(makeBookingRoom(), RoomKind.SUITE, new BigDecimal(3.0), 8);
        assertEquals(bookingDescription.getCost(), bookingDescription.getFixedPrice().multiply(new BigDecimal(8.0)));
    }

    @Test
    public void testCostRoom() {
        BookingDescription bookingDescription = new BookingDescription(makeBookingRoom(), RoomKind.SUITE, new BigDecimal(3.0), 1);

        assertEquals(bookingDescription.getCost().doubleValue(), bookingDescription.getFixedPrice().doubleValue(), 0.001);

    }


    private RoomType makeBookingRoom () {
        return new RoomType("Wedding Room");
    }

}
