package com.vladter.Tests;

import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.RoomClasses.RoomType;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Created by ��������� on 12.11.2015.
 */
public class RoomTypeTests {
    private RoomType makeRoomType() {
        return new RoomType("Wedding Room");
    }

    @Test
    public void createRoomTest() {
        RoomType roomType = makeRoomType();

        assertEquals(roomType.getName(), "Wedding Room");
        assertEquals(roomType.getDescription(), "");
        assertEquals(roomType.getImageURL(), "");
        assertEquals(roomType.getVotes(), 0);

    }

    @Test (expected = Exception.class)
    public void positive_ratingTest(){
        RoomType roomType = makeRoomType();

        roomType.rate(1);
    }

    @Test (expected = Exception.class)
    public void negative_ratingTest() {
        RoomType roomType = makeRoomType();

        roomType.rate(-1);
    }

   /*@Test
    public void pricesZeroTest() {
       RoomType roomType = makeRoomType();

       assertEquals(roomType.getCurrentPrice(RoomKind.STANDART), new BigDecimal(0.0));
       assertEquals(roomType.getCurrentPrice(RoomKind.SUITE), new BigDecimal(0.0));
       assertEquals(roomType.getCurrentPrice(RoomKind.LUXE), new BigDecimal(0.0));
   }
*/
    @Test
    public void pricesAfterUpdateTest() {

        RoomType roomType = makeRoomType();

        roomType.updatePrice(RoomKind.STANDART, new BigDecimal(25.0));
        roomType.updatePrice(RoomKind.SUITE, new BigDecimal(30.0));
        roomType.updatePrice(RoomKind.LUXE, new BigDecimal(50.0));

        assertEquals(roomType.getCurrentPrice(RoomKind.STANDART), new BigDecimal(25.0));
        assertEquals(roomType.getCurrentPrice(RoomKind.SUITE), new BigDecimal(30.0));
        assertEquals(roomType.getCurrentPrice(RoomKind.LUXE), new BigDecimal(50.0));
    }

    @Test
    public void nameChangeTest() {
        RoomType roomType = new RoomType("Wedding Room");
        roomType.rename("Not-Wedding Room");
        assertEquals(roomType.getName(), "Not-Wedding Room");
    }


}
