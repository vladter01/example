package com.vladter.Tests;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.BookingClasses.BookingCard;
import com.vladter.Model.BookingClasses.BookingDescription;
import com.vladter.Model.GuestContact;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.PayClasses.CashPaymentPlan;
import com.vladter.Model.PayClasses.PaymentPlan;
import com.vladter.Model.RoomClasses.Room;
import com.vladter.Model.RoomClasses.RoomType;
import com.vladter.Model.StatusEnums.BookingStatus;
import junit.framework.TestCase;
import org.junit.*;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertSame;

public class BookingTests {

    @Test
    public void constructorCorrectTest() {
        GuestContact guestContact = new GuestContact("", "", "");
        PaymentPlan paymentPlan = new CashPaymentPlan();

        Booking b = new Booking(guestContact, paymentPlan);


        assertSame(b.getGuestContact(), guestContact);
        assertSame(b.getPaymentPlan(), paymentPlan);
    }

    @Test
    public void totalCostTest() {
        Booking b = makeBooking();
        b.setDiscount(0.6);

        assertEquals(b.getDiscount(), 0.6);
    }

    @Test
    public void totalCostWithoutDiscount() {
        Booking b = makeBooking();
        b.setDiscount(0.0);

        assertEquals(b.getTotalCost(), b.getBookingCard().getCost());
    }

    @Test
    public void totalCostWithFullDiscount() {
        Booking b = makeBooking();
        b.setDiscount(1.0);

        assertEquals(b.getTotalCost(), BigDecimal.ZERO);
    }

    @Test(expected = Exception.class)
    public void checkDiscountTest() {
        Booking b = makeBooking();
        b.setDiscount(0.6);
        TestCase.assertEquals(b.getDiscount(), 0.6, 0.001);
    }

    @Test
    public void booking_newStatusTest() {
        Booking b = makeBooking();
        TestCase.assertEquals(b.getBookingStatus(), BookingStatus.NEW);

    }

    @Test
    public void cancel_NewBookingTest() {
        Booking b = makeBooking();
        b.cancel();

        TestCase.assertEquals(b.getBookingStatus(), BookingStatus.NEW);

    }



    private static Booking makeBooking() {
        BookingCard card = new BookingCard();
        card.addDescription(makeSomeDesc());

        return makeBooking(card);
    }

    private static Booking makeBooking(BookingCard card) {
        return new Booking(card, new GuestContact("","",""), new CashPaymentPlan());
    }

    private static Booking makeBooking(PaymentPlan plan) {
        BookingCard card = new BookingCard();
        card.addDescription(makeSomeDesc());
        return new Booking(card, new GuestContact("", "",""), plan);
    }

    private static BookingDescription makeSomeDesc(int amount) {
        return new BookingDescription(new RoomType("Wedding Room"), RoomKind.STANDART, new BigDecimal(1.0) , 1);

    }

    private static BookingDescription makeSomeDesc() {
        return makeSomeDesc(1);

    }

    private static BookingDescription makeOtherDesc(int amount) {
        return new BookingDescription(new RoomType("Family Room"), RoomKind.LUXE, new BigDecimal(2.0), amount);


    }

    private static void BookingRoom (Room room) {

        room.onBookingRoom();
        room.onCancelledBookingRoom();

    }




}
