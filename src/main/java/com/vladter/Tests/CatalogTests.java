package com.vladter.Tests;

import com.vladter.Model.Catalog;
import com.vladter.Model.ConferenceClasses.ConferenceType;
import com.vladter.Model.RoomClasses.RoomType;
import org.junit.Test;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class CatalogTests {

    @Test
    public void testCatalog() {
        CatalogTests catalogTests = new CatalogTests();
    }

    @Test
    public void testFindRoom() {
        Catalog c = new Catalog();

        RoomType weddingRoom = new RoomType("Wedding Room");
        c.addRoomType(weddingRoom);

        assertEquals(c.getRoomTypesAmount(), 1);
        assertSame(c.findRoomType("Wedding Room"), weddingRoom);
    }

    @Test
    public void testFindHall() {
        Catalog c = new Catalog();

        ConferenceType discussHall = new ConferenceType("Discussion Hall");
        c.addHallType(discussHall);

        assertEquals(c.getHallTypesAmount(), 1);
        assertSame(c.findHallType("Discussion Hall"), discussHall);
    }

    @Test
    public void testDeleteRoom() {
        Catalog c = new Catalog();

        RoomType weddingRoom = new RoomType("Wedding Room");
        c.addRoomType(weddingRoom);
        c.deleteRoomType("Wedding Room");

        assertEquals(c.getHallTypesAmount(), 0);
    }

    @Test
    public void testDeleteHall() {
        Catalog c = new Catalog();

        ConferenceType discussHall = new ConferenceType("Discussion Hall");
        c.addHallType(discussHall);
        c.deleteHallType("Discussion Hall");

        assertEquals(c.getHallTypesAmount(), 0);
    }

    @Test
    public void testFindRoom_returnNull() {
        Catalog c = new Catalog();

        c.addRoomType(new RoomType("Wedding Room"));
        assertNull(c.findRoomType("Family Room"));
    }

    @Test
    public void testFindHall_returnNull() {
        Catalog c = new Catalog();

        c.addHallType(new ConferenceType("Discuss Hall"));
        assertNull(c.findHallType("Teambuilding Hall"));
    }

    @Test( expected = Exception.class)
    public void testDuplicateRoom_Fails() {
        Catalog c = new Catalog();
        c.addRoomType(new RoomType("Wedding Room"));
        c.addRoomType(new RoomType("Wedding Room"));
    }

    @Test (expected =  Exception.class)
    public void testDuplicateHall_Fails() {
        Catalog c = new Catalog();
        c.addHallType(new ConferenceType("Discuss Hall"));
        c.addHallType(new ConferenceType("Discuss Hall"));
    }


}
