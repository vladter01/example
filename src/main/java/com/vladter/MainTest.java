package com.vladter;

import com.vladter.Model.Catalog;
import com.vladter.Model.SunRise;
import com.vladter.Test.Body;
import com.vladter.Test.Test;

public class MainTest {

    public static void main(String[] args) {

        MainTest m = new MainTest();
        m.fillTest();
        m.fillBody();

    }

    public void fillTest() {
        Test t = new Test();
        sunRise = t.generate();
    }


    public void fillBody() {
        Body b = new Body(System.out, sunRise);
        b.generate();
    }

    private SunRise sunRise = new SunRise();


}
