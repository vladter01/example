package com.vladter.Test;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.Catalog;
import com.vladter.Model.ConferenceClasses.ConferenceType;
import com.vladter.Model.GuestContact;
import com.vladter.Model.HotelHistory;
import com.vladter.Model.KindEnums.ConferenceKind;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.PayClasses.CashPaymentPlan;
import com.vladter.Model.RoomClasses.RoomType;
import com.vladter.Model.SunRise;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by ��������� on 03.11.2015.
 */
public class Test {

    public SunRise generate() {
        SunRise sunRise = new SunRise();
        fillHallCatalog(sunRise.getCatalog());
        fillRoomCatalog(sunRise.getCatalog());


        return sunRise;
    }

    private void fillRoomCatalog (Catalog c) {
        RoomType weddingRoom = new RoomType("Wedding Room");
        c.addRoomType(weddingRoom);

        weddingRoom.updateDescription("Lovely room. Perfect for the first night together.");
        weddingRoom.setRating(120);
        weddingRoom.setImageURL("sun_rise.ua/weddingRoom");

        weddingRoom.updatePrice(RoomKind.SUITE, new BigDecimal(2.00, money));
        weddingRoom.updatePrice(RoomKind.STANDART, new BigDecimal(3.00, money));
        weddingRoom.updatePrice(RoomKind.LUXE, new BigDecimal(7.00, money));


        RoomType familyRoom = new RoomType("Family Room");
        c.addRoomType(familyRoom);

        familyRoom.updateDescription("Perfect for a young family and family as a whole. ");
        familyRoom.setRating(56.8);
        familyRoom.setImageURL("sun_rise.ua/room_catalog/familyRoom");

        familyRoom.updatePrice(RoomKind.STANDART, new BigDecimal(3.00, money));
        familyRoom.updatePrice(RoomKind.SUITE, new BigDecimal(5.00, money));
        familyRoom.updatePrice(RoomKind.LUXE, new BigDecimal(10.00, money));

    }

    private void fillHallCatalog (Catalog c) {
        ConferenceType discussHall = new ConferenceType("Discussion Hall");
        c.addHallType(discussHall);

        discussHall.updateDescription("Good hall for communication");
        discussHall.setRating(64.9);
        discussHall.setImageURL("sun_rise.ua/hall_catalog/discussHall");

        discussHall.updatePrice(ConferenceKind.SMALL, new BigDecimal(15, money));
        discussHall.updatePrice(ConferenceKind.BIG, new BigDecimal(25, money));


        ConferenceType teambuildingHall = new ConferenceType("Team Building Hall");
        c.addHallType(teambuildingHall);

        teambuildingHall.updateDescription("Perfect for team building");
        teambuildingHall.setRating(64.9);
        teambuildingHall.setImageURL("sun_rise.ua/hall_catalog/teambuildingHall");

        teambuildingHall.updatePrice(ConferenceKind.SMALL, new BigDecimal(13, money));
        teambuildingHall.updatePrice(ConferenceKind.BIG, new BigDecimal(27, money));

    }

    private void fillBooking (SunRise sunRise, Catalog c) {
        RoomType weddingRoom = c.findRoomType("Wedding Room");
        RoomType familyRoom = c.findRoomType("Family Room");

        ConferenceType teambuildingHall = c.findHallType("Team Building Hall");
        ConferenceType discussHall = c.findHallType("Discuss Hall");

        Booking booking1 = sunRise.newBooking( new GuestContact("Illya Vasilyev", "illia@gmail.com", "+38093-103-1111"), new CashPaymentPlan());
        booking1.getBookingCard().addDescription(c.makeBookingRoom(weddingRoom, RoomKind.STANDART, 1));

        Booking booking2 = sunRise.newBooking( new GuestContact("Petr Izumrudov", "petr@gmail.com", "+38095-103-5555"), new CashPaymentPlan());
        booking2.getBookingCard().addDescription(c.makeBookingHall(discussHall, ConferenceKind.SMALL, 1));


    }

    private void fillHistory(HotelHistory history) {

        history.setInfo("The hotel was founded in 1843. Located on the shores of the Pacific Ocean. Sun Rise is known for famous visitors, who stayed here during the entire existence of the hotel.");
        history.setOldphotoURL("sun_rise.ua/oldphotoUrl");
    }

    private static MathContext money = new MathContext(3);
}
