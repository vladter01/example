package com.vladter.Test;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.BookingClasses.BookingCard;
import com.vladter.Model.BookingClasses.BookingDescription;
import com.vladter.Model.Catalog;
import com.vladter.Model.ConferenceClasses.ConferenceType;
import com.vladter.Model.GuestContact;
import com.vladter.Model.KindEnums.ConferenceKind;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.RoomClasses.RoomType;
import com.vladter.Model.SunRise;
import com.vladter.Model.*;
import java.io.PrintStream;

/**
 * Created by ��������� on 04.11.2015.
 */

public class Body {
    private PrintStream ps;
    private SunRise sunRise;

    public Body( PrintStream ps, SunRise sunRise) {
        this.ps = ps;
        this.sunRise = sunRise;
    }

    public void generate() {
        showCatalog(sunRise.getCatalog());
        showBookings(sunRise.getBookings());
    }

    private void showCatalog (Catalog m) {
        ps.println("....SunRise Catalog....");

        for(String name : m.getHallTypeNames()) {

            ConferenceType conferenceType = m.findHallType(name);
            ps.println();
            ps.println("Conference Hall \"" + name + "\" :");
            ps.println("\tDescription : " + conferenceType.getDescription());
            ps.println("\tImage : " + conferenceType.getImageURL());

            showHallPrices(conferenceType);
            showHallRating(conferenceType);

        }

        for(String name : m.getRoomTypeNames()) {
            RoomType roomType = m.findRoomType(name);
            ps.println();
            ps.println("Room \"" + name + "\" :");
            ps.println("\tDesciption : " + roomType.getDescription());
            ps.println("\tImage : " + roomType.getImageURL());

            showRoomPrices(roomType);
            showRoomRating(roomType);
        }

        ps.println();
        ps.println("....End Catalog....");

    }

    public void showRoomRating (RoomType roomType) {

        ps.print("\tRoom rating : ");
        ps.print(roomType.getRating());


    }

    private void showHallRating (ConferenceType conferenceType) {

        ps.println("\tConference Hall rating : ");
        ps.print(conferenceType.getRating());

    }

    private void showRoomPrices (RoomType roomType) {
        ps.print("\tRoom prices : ");
        ps.print(roomType.getCurrentPrice(RoomKind.STANDART));
        ps.print(" standart, ");
        ps.print(roomType.getCurrentPrice(RoomKind.SUITE));
        ps.print(" suite, ");
        ps.print(roomType.getCurrentPrice(RoomKind.LUXE));
        ps.println(" luxe");

    }

    private void showHallPrices (ConferenceType conferenceType) {
        ps.print("\tConference Hall prices : ");
        ps.print(conferenceType.getCurrentPrice(ConferenceKind.SMALL));
        ps.print(" small, ");
        ps.print(conferenceType.getCurrentPrice(ConferenceKind.BIG));
        ps.print(" big");
    }

    private void showBookings ( Iterable<Booking> bookings) {

        ps.print("....Reservations....");

        for (Booking booking : bookings) {
            showBooking(booking);
        }
        ps.println();
        ps.println("....End Reservations....");
    }

    private void showBooking (Booking booking ) {
        ps.println();
        ps.print("Booking : ");

        ps.print("\tStatus : ");
        ps.println(booking.getBookingStatus().name());

        ps.print("\tTotal cost : ");
        ps.println(booking.getTotalCost());

        ps.print("\tDiscount : ");
        ps.println(booking.getDiscount() * 100.0);
        ps.println('%');

        ps.print("\tPayment plan : ");
        ps.println(booking.getPaymentPlan());

        showCart(booking.getBookingCard());
        showGuestContact(booking.getGuestContact());
    }

    private void showCart (BookingCard bookingCard) {
        ps.println("\tCart : ");

        int n = bookingCard.getDescriptionCount();
        for (int i = 0; i < n; i ++ ) {
            ps.print("\t  ");
            ps.print(i + 1);
            ps.print(") ");

            BookingDescription bookingDescription = bookingCard.getDescription(i);

            ps.print(bookingDescription.getRoomType().getName());
            ps.print(", price");
            ps.print(bookingDescription.getFixedPrice());
            ps.print(", amount ");
            ps.print(bookingDescription.getAmount());
            ps.print(", cost ");
            ps.print(bookingDescription.getCost());



        }
    }

    private void showGuestContact (GuestContact guestContact ) {
        ps.print("\tName : ");
        ps.println(guestContact.getName());

        ps.print("\tPhone number : ");
        ps.println(guestContact.getPhoneNumber());

        ps.print("\tEmail : ");
        ps.println(guestContact.getEmail());
    }

    private void showHistory (HotelHistory hotelHistory) {
        ps.print("\tHistory : ");
        ps.println(hotelHistory.getInfo());

        ps.print("\tPhotos : ");
        ps.println(hotelHistory.getOldphotoURL());
    }


}
