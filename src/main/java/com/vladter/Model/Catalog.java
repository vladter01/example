package com.vladter.Model;

import com.vladter.Model.BookingClasses.BookingDescription;
import com.vladter.Model.ConferenceClasses.ConferenceType;
import com.vladter.Model.KindEnums.ConferenceKind;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.RoomClasses.RoomType;

import java.util.Map;
import java.util.TreeMap;


public class Catalog implements RoomType.NameChange {
    private Map<String, RoomType> typeRoomByName = new TreeMap<String , RoomType>();
    private Map<String, ConferenceType> typeHallByName = new TreeMap<String, ConferenceType>();



    public Map<String, ConferenceType> getTypeHallByName() {
        return typeHallByName;

    }

    public Iterable<String> getHallTypeNames() {
        return typeHallByName.keySet();
    }

    public Iterable<String> getRoomTypeNames() {
        return typeRoomByName.keySet();
    }

    public Iterable<RoomType> roomTypes() {
        return typeRoomByName.values();
    }

    public Iterable<ConferenceType> conferenceTypes() {
        return typeHallByName.values();
    }





    public int getRoomTypesAmount () {
        return typeRoomByName.size();
    }
    public int getHallTypesAmount () { return typeHallByName.size(); }


    public RoomType findRoomType (String name ) {
        return typeRoomByName.get(name);
    }

    public ConferenceType findHallType (String name ) {
        return typeHallByName.get(name);
    }

    public void addRoomType(RoomType roomType) {

        if(typeRoomByName.containsValue(roomType)) {
            throw new RuntimeException("Types should be different.");
        }

        typeRoomByName.put(roomType.getName(), roomType);
    }

    public void addHallType (ConferenceType conferenceType) {

        if (typeHallByName.containsValue(conferenceType)) {
            throw new RuntimeException("Types should be different.");
        }

        typeHallByName.put(conferenceType.getName(), conferenceType);
    }


    public void deleteRoomType (String name) {
        if (typeRoomByName.containsKey(name)) {
            typeRoomByName.remove(name);
        }
    }
    public void deleteHallType (String name) {
        if (typeHallByName.containsKey(name)) {
            typeHallByName.remove(name);
        }
    }


    public BookingDescription makeBookingRoom(RoomType roomType, RoomKind roomKind, int amount) {
        return new BookingDescription(roomType, roomKind, roomType.getCurrentPrice(roomKind),amount);
    }

    public BookingDescription makeBookingHall(ConferenceType conferenceType, ConferenceKind conferenceKind, int amount) {
        return new BookingDescription(conferenceType, conferenceKind, conferenceType.getCurrentPrice(conferenceKind), amount);
    }

    @Override
    public void roomTypeNameChanged(String oldname, String newName) {
        RoomType roomType = typeRoomByName.get(oldname);
        if(roomType == null ) throw new RuntimeException("Unregistered kind");

        if(oldname == newName)
            return;

        if (findRoomType(newName) != null) throw new RuntimeException("Target name already reserved");

        typeRoomByName.remove(oldname);
        typeRoomByName.put(newName, roomType);
    }


}
