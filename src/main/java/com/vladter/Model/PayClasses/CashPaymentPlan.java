package com.vladter.Model.PayClasses;

/**
 * Created by ��������� on 31.10.2015.
 */
public class CashPaymentPlan extends PaymentPlan {

    public boolean payed;

    public CashPaymentPlan() {
        this.payed = false;
    }

    public boolean isPayed() {
        return payed;
    }


    @Override
    public PaymentPlan clone() {

        CashPaymentPlan copy = new CashPaymentPlan();
        copy.setPayed( wasPayed());
        return copy;

    }

    @Override
    public boolean expectPrepayment() {
        return false;
    }

    @Override
    public boolean wasPayed() {
        return payed;
    }

    public void setPayed ( boolean payed) {
        this.payed = payed;
    }

    public String ToString() {
        return "Cash payment on delivery";
    }
}
