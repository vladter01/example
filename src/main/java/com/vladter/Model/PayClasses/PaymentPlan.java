package com.vladter.Model.PayClasses;

/**
 * Created by ��������� on 31.10.2015.
 */
public abstract class PaymentPlan {
    public abstract PaymentPlan clone();
    public abstract boolean expectPrepayment();
    public abstract boolean wasPayed();
}
