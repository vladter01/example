package com.vladter.Model.StatusEnums;

/**
 * Created by ��������� on 18.10.2015.
 */
public enum BookingStatus {
    NEW,
    REGISTERED,
    PREPAID,
    PAID;
}
