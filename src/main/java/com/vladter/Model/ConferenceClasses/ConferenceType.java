package com.vladter.Model.ConferenceClasses;

import com.vladter.Model.KindEnums.ConferenceKind;

import java.math.BigDecimal;

/**
 * Created by ��������� on 18.10.2015.
 */
public class ConferenceType {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public ConferenceType(String name) {
        this.name = name;
        this.description = "";
        this.imageURL = "" ;
        this.votes = 0;
        this.rating = 0.0;

    }

    public BigDecimal getPrice(ConferenceKind conferenceKind, BigDecimal price) {
        return priceBySize[conferenceKind.ordinal()];
    }

    public BigDecimal getCurrentPrice(ConferenceKind conferenceKind) {
        return priceBySize[conferenceKind.ordinal()];
    }

    public void updateDescription (String description) {
        this.description = description;
    }


    public BigDecimal updatePrice(ConferenceKind conferenceKind, BigDecimal price) {
        return priceBySize[conferenceKind.ordinal()] = price;
    }

    public void rate() {
        this.rating += rating;
        this.votes++;
    }

    private String name;
    private String description;
    private String imageURL;
    private double rating;
    private int votes;

    private BigDecimal[] priceBySize = new BigDecimal[ConferenceKind.values().length];
}
