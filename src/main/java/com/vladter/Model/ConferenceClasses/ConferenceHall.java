package com.vladter.Model.ConferenceClasses;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.KindEnums.ConferenceKind;
import com.vladter.Model.StatusEnums.ConferenceStatus;

/**
 * Created by ��������� on 03.11.2015.
 */
public class ConferenceHall {

    private ConferenceKind conferenceKind;
    private ConferenceType conferenceType;
    private Booking booking;
    private ConferenceStatus conferenceStatus;


    public ConferenceKind getConferenceKind() {
        return conferenceKind;
    }

    public ConferenceType getConferenceType() {
        return conferenceType;
    }

    public Booking getBooking() {
        return booking;
    }

    public ConferenceStatus getConferenceStatus() {
        return conferenceStatus;
    }



    public ConferenceHall (ConferenceKind conferenceKind, ConferenceType conferenceType, Booking booking) {
        this.conferenceKind = conferenceKind;
        this.conferenceType = conferenceType;
        this.booking = booking;
        this.conferenceStatus = ConferenceStatus.FREE;

    }

    public void onBookingHall() {
        if(conferenceStatus == ConferenceStatus.FREE) {
            conferenceStatus = ConferenceStatus.RESERVE;
            booking.onBookingRegistered();
        } else {
            throw new RuntimeException("Must be paid if war reserve");
        }
    }

}
