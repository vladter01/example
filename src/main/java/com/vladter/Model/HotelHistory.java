package com.vladter.Model;

/**
 * Created by ��������� on 09.11.2015.
 */
public class HotelHistory {

    private String info;
    private String oldphotoURL;

    public HotelHistory() {
        this.info = "";
        this.oldphotoURL = "";
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public void setOldphotoURL(String oldphotoURL) {
        this.oldphotoURL = oldphotoURL;
    }

    public String getOldphotoURL() {
        return oldphotoURL;
    }

    public void editInfo(String info) {
        this.info = info;
    }
}
