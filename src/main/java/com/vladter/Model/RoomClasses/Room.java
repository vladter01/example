package com.vladter.Model.RoomClasses;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.StatusEnums.RoomStatus;

/**
 * Created by ��������� on 31.10.2015.
 */
public class Room {
    private RoomType roomType;
    private RoomKind roomKind;
    private Booking booking;
    private RoomStatus roomStatus;

    public Room(RoomType roomType, RoomKind roomKind, Booking booking) {
        this.roomKind = roomKind;
        this.roomType = roomType;
        this.booking = booking;
        this.roomStatus = RoomStatus.FREE;
    }

    public Room(RoomType roomType, RoomKind roomKind){
        this.roomType = roomType;
        this.roomKind = roomKind;
    }
    public RoomType getRoomType() {
        return roomType;
    }

    public RoomKind getRoomKind() {
        return roomKind;
    }

    public Booking getBooking() {
        return booking;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void onBookingRoom() {
        if (roomStatus == RoomStatus.FREE) {
            roomStatus = RoomStatus.RESERVE;
            booking.onBookingRegistered();
        } else {
            throw new RuntimeException("May only registered");
        }
    }

    public void onCancelledBookingRoom () {
        if (roomStatus == RoomStatus.RESERVE || roomStatus == RoomStatus.BUSY) {
            roomStatus = RoomStatus.FREE;
        } else {
            throw new RuntimeException("Must be paid if was reserve");
        }
    }


}
