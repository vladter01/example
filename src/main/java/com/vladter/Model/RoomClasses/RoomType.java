package com.vladter.Model.RoomClasses;

import com.vladter.Model.KindEnums.RoomKind;

import javax.lang.model.element.Name;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ��������� on 18.10.2015.
 */
public class RoomType {

    public interface NameChange {
        void roomTypeNameChanged (String oldname, String newName);
    }


    public RoomType(String name) {

        this.nameChangeList = new ArrayList<NameChange>();

        int nPrices = RoomKind.values().length;
        this.priceBySize = new BigDecimal[nPrices];
        for(int i = 0; i < nPrices; i++) {
            this.priceBySize[i] = BigDecimal.ZERO;

        }

        this.name = name;
        this.description = "";
        this.imageURL = "";
        this.votes = 0;
        this.rating = 0.0;

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public BigDecimal getPrice(RoomKind roomKind, BigDecimal price) {
        return priceBySize [roomKind.ordinal()];
    }

    public void updatePrice (RoomKind roomKind, BigDecimal price) {
        priceBySize[roomKind.ordinal()] = price;
    }

    public void rate(int rating) {
        this.rating += rating;
        this.votes++;
    }

    public void addNameChangeList (NameChange changer) {
        nameChangeList.add(changer);
    }

    public void removeNameChangeList (NameChange changer) {
        nameChangeList.remove(changer);
    }

    public void rename(String value) {
        if (value == null || value.isEmpty()) throw new RuntimeException("Empty name");

        if (name != value)
            for (NameChange n : nameChangeList) {
                n.roomTypeNameChanged(name, value);
            }


        name = value;
    }
    public void updateDescription (String description) {
        this.description = description;
    }

    public BigDecimal getCurrentPrice( RoomKind kind )
    {
        return priceBySize[ kind.ordinal() ];
    }

    private String name;
    private String description;
    private String imageURL;
    private int votes;
    private double rating;
    private BigDecimal[] priceBySize = new BigDecimal[RoomKind.values().length];
    private List<NameChange> nameChangeList;

}
