package com.vladter.Model;

import com.vladter.Model.BookingClasses.Booking;
import com.vladter.Model.BookingClasses.BookingCard;
import com.vladter.Model.PayClasses.PaymentPlan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ��������� on 03.11.2015.
 */
public class SunRise {

    private Catalog catalog = new Catalog();
    private List<Booking> bookingList = new ArrayList<Booking>();


    public Catalog getCatalog() {
        return catalog;
    }

    public int getBookingCount () {
        return bookingList.size();
    }

    public Booking newBooking(GuestContact guestContact, PaymentPlan paymentPlan) {
        Booking b = new Booking(guestContact, paymentPlan);
        bookingList.add(b);
        return b;

    }

    public void addBooking (Booking booking) {
        bookingList.add(booking);
    }

    public Iterable <Booking> getBookings () {
        return  bookingList;
    }
}
