package com.vladter.Model.BookingClasses;

import com.vladter.Model.ConferenceClasses.ConferenceType;
import com.vladter.Model.KindEnums.ConferenceKind;
import com.vladter.Model.KindEnums.RoomKind;
import com.vladter.Model.RoomClasses.RoomType;

import java.math.BigDecimal;

/**
 * Created by ��������� on 18.10.2015.
 */
public class BookingDescription {
    public BookingDescription(RoomType roomType, RoomKind roomKind, BigDecimal fixedPrice, int amount) {
        this.fixedPrice = fixedPrice;
        this.amount = amount;
        this.roomType = roomType;
        this.roomKind = roomKind;
    }

    public BookingDescription(ConferenceType conferenceType, ConferenceKind conferenceKind, BigDecimal fixedPrice, int amount){
        this.amount = amount;
        this.conferenceKind = conferenceKind;
        this.conferenceType = conferenceType;
        this.fixedPrice = fixedPrice;
    }



    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public BigDecimal getFixedPrice() {
        return fixedPrice;
    }

    public void setFixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public BigDecimal getCost() {

       return fixedPrice.multiply(new BigDecimal(amount));
    }

    public RoomKind getRoomKind() {
        return roomKind;
    }

    public ConferenceType getConferenceType() {
        return conferenceType;
    }

    public void setConferenceType(ConferenceType conferenceType) {
        this.conferenceType = conferenceType;
    }

    public ConferenceKind getConferenceKind() {
        return conferenceKind;
    }

    public void setConferenceKind(ConferenceKind conferenceKind) {
        this.conferenceKind = conferenceKind;
    }

    public void setRoomKind(RoomKind roomKind) {
        this.roomKind = roomKind;
    }

    private RoomType roomType;
    private BigDecimal fixedPrice;
    private int amount;
    private RoomKind roomKind;

    private ConferenceType conferenceType;
    private ConferenceKind conferenceKind;


}
