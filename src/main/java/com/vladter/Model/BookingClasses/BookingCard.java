package com.vladter.Model.BookingClasses;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
/**
 * Created by ��������� on 18.10.2015.
 */


public class BookingCard {
    private Integer id;
    private Integer variable;

    public Integer getVariable() {
        return variable;
    }

    public void setVariable(Integer variable) {
        this.variable = variable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BookingCard() {
        this.bookingDescriptionList = new ArrayList<BookingDescription>();
    }

    public int getDescriptionCount () {
        return bookingDescriptionList.size();
    }

    public BookingDescription getDescription(int index) {
        return bookingDescriptionList.get(index);
    }

    public void addDescription(BookingDescription bookingDescription) {
        bookingDescriptionList.add(bookingDescription);
    }

    public void removeDescription(int index) {
        bookingDescriptionList.remove(index);
    }

    public void updateDescription(int index, BookingDescription bookingDescription) {
        bookingDescriptionList.add(index, bookingDescription);
    }

    public  void clear (BookingDescription bookingDescription) {
        bookingDescriptionList.clear();
    }

    public BigDecimal getCost() {
       BigDecimal totalCost = new BigDecimal(0);
        for (BookingDescription i : bookingDescriptionList)
            totalCost.add(i.getCost());
        return totalCost;
    }

    private List<BookingDescription> bookingDescriptionList ;
}
