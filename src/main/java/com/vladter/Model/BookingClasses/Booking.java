package com.vladter.Model.BookingClasses;

import com.vladter.Model.BookingClasses.BookingCard;
import com.vladter.Model.BookingClasses.BookingDescription;
import com.vladter.Model.ConferenceClasses.ConferenceHall;
import com.vladter.Model.GuestContact;
import com.vladter.Model.PayClasses.PaymentPlan;
import com.vladter.Model.RoomClasses.Room;
import com.vladter.Model.StatusEnums.BookingStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ��������� on 31.10.2015.
 */
public class Booking {
    private BookingCard bookingCard;
    private GuestContact guestContact;
    private BookingStatus bookingStatus;
    private PaymentPlan paymentPlan;
    private double discount;
    private List<Room> roomList;
    private List<ConferenceHall> conferenceList;

    public BookingCard getBookingCard() {
        return bookingCard;
    }

    public void setBookingCard(BookingCard bookingCard) {
        this.bookingCard = bookingCard;
    }

    public GuestContact getGuestContact() {
        return guestContact;
    }

    public void setGuestContact(GuestContact guestContact) {
        this.guestContact = guestContact;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        if (discount < 0.0 || discount > 1.0) throw new RuntimeException ("Invalid value of discount");


        this.discount = discount;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public BigDecimal getTotalCost () {
        BigDecimal cost = bookingCard.getCost();

        return (cost.multiply(new BigDecimal( 1.0 - discount)));
    }


    public void onBookingRegistered () {
        if (bookingStatus == BookingStatus.REGISTERED)
            bookingStatus = BookingStatus.PREPAID;

        else if (bookingStatus != BookingStatus.PREPAID )
            throw new RuntimeException("Can only happen in Registered & Prepaid states");

    }


    public void cancel() {

        switch ( bookingStatus ) {
            case NEW:
            case REGISTERED:
            case PREPAID:
            case PAID:
                break;
            default:
                throw new RuntimeException("Cannot cancel");
        }
        bookingStatus = BookingStatus.NEW;

    }

    public void processRoom() {
        if (bookingStatus == BookingStatus.NEW) throw new RuntimeException("Can only process new Booking");
        bookingStatus = BookingStatus.REGISTERED;

        int n = bookingCard.getDescriptionCount();
        for (int i = 0; i < n; i ++) {
            BookingDescription description = bookingCard.getDescription(i);
            for (int j = 0; j < description.getAmount(); j++) {
                roomList.add(new Room(description.getRoomType(), description.getRoomKind(), this));
            }
        }
    }

    public void processHall() {
        if (bookingStatus == BookingStatus.NEW) throw new RuntimeException("Can only process new Booking");
        bookingStatus = BookingStatus.REGISTERED;

        int n = bookingCard.getDescriptionCount();
        for (int i = 0; i < n; i ++) {
            BookingDescription description = bookingCard.getDescription(i);
            for (int j = 0; j < description.getAmount(); j ++) {
                conferenceList.add(new ConferenceHall(description.getConferenceKind(), description.getConferenceType(), this));
            }
        }
    }


    public Booking (GuestContact guestContact, PaymentPlan paymentPlan) {
        this.guestContact = guestContact;
        this.paymentPlan = paymentPlan;
        this.bookingCard = new BookingCard();
        this.bookingStatus = BookingStatus.NEW;
        this.discount = 0.0;
        this.roomList = new ArrayList<Room>();
    }

    public Booking (BookingCard bookingCard, GuestContact guestContact, PaymentPlan paymentPlan) {
        this.guestContact = guestContact;
        this.paymentPlan = paymentPlan;
        this.bookingCard = new BookingCard();
        this.bookingStatus = BookingStatus.NEW;
        this.discount = 0.0;
        this.roomList = new ArrayList<Room>();
    }

}
